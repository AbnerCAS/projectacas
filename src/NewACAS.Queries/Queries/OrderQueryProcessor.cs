﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewACAS.API.Common.Exception;
using NewACAS.API.Models;
using NewACAS.Data.Access.DAL;
using NewACAS.Data.Model.Customer;
using NewACAS.Data.Model.Order;

namespace NewACAS.Queries.Queries
{
    public class OrderQueryProcessor : IOrderQueryProcessor
    {
        private readonly IUnitOfWork _uow;

        public OrderQueryProcessor(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IQueryable<Order> Get()
        {
            throw new NotImplementedException();
        }

        public Order Get(int id)
        {
            var order = _uow.Query<Order>().FirstOrDefault(x => x.Id == id);

            if (order == null)
            {
                throw new NotFoundException("Order not found");
            }

            return order;
        }

        public async Task<Order> Create(CreateOrder model)
        {
            if (model.CustomerId <= 0)
                throw  new BadRequestException("The Customer Id is required");

            var customer = _uow.Query<Customer>().FirstOrDefault(c => c.Id == model.CustomerId);

            if(customer == null)
                throw new BadRequestException("The Customer do not exist.");

            var item = new Order()
            {
                Price = model.Price,
                CreatedAt = DateTime.Now,
                CustomerId = model.CustomerId,
                ProductName = model.ProductName,
                ProductType = model.ProductType
            };

            _uow.Add(item);
            await _uow.CommitAsync();

            return item;
        }

        public async Task Delete(int id)
        {
            var order = _uow.Query<Order>().FirstOrDefault(u => u.Id == id);

            if (order == null)
            {
                throw new NotFoundException("The Order is not found");
            }

            if (order.IsDeleted) return;

            order.IsDeleted = true;
            await _uow.CommitAsync();
        }
    }
}
