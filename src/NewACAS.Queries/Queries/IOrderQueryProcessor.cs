﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewACAS.API.Models;
using NewACAS.Data.Model.Order;

namespace NewACAS.Queries.Queries
{
    public interface IOrderQueryProcessor
    {
        IQueryable<Order> Get();
        Order Get(int id);
        Task<Order> Create(CreateOrder model);
        Task Delete(int id);
    }
}
