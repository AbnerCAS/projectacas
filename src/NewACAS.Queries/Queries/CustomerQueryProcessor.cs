﻿using System;
using System.Linq;
using System.Threading.Tasks;

using NewACAS.Data.Access.DAL;
using NewACAS.Data.Model;
using Microsoft.EntityFrameworkCore;
using NewACAS.API.Common.Exception;
using NewACAS.API.Models;
using NewACAS.Data.Model.Customer;

namespace NewACAS.Queries.Queries
{
    public class CustomerQueryProcessor : ICustomerQueryProcessor
    {
        private readonly IUnitOfWork _uow;

        public CustomerQueryProcessor(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public Customer Get(int id)
        {
            var user = _uow.Query<Customer>().FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                throw new NotFoundException("Customer is not found");
            }

            return user;
        }

        public async Task<Customer> Create(CreateCustomer model)
        {
            var item = new Customer()
            {
                Active = true,
                CreatedAt = DateTime.Now,
                BirthDate = model.BirthDate,
                Document = model.Document,
                Email = model.Email,
                FirstName = model.FirstName,
                SecondName = model.SecondName
            };

            _uow.Add(item);
            await _uow.CommitAsync();

            return item;
        }

        public async Task Delete(int id)
        {
            var user = _uow.Query<Customer>().FirstOrDefault(u => u.Id == id);

            if (user == null)
            {
                throw new NotFoundException("Customer is not found");
            }
            user.Active = true;
            await _uow.CommitAsync();
        }

    }
}
