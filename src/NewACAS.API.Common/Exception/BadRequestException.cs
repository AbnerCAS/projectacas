﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewACAS.API.Common.Exception
{
    public class BadRequestException : System.Exception
    {
        public BadRequestException(string message) : base(message)
        {
        }
    }
}
