﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewACAS.API.Common.Exception
{
    public class ForbiddenException : System.Exception
    {
        public ForbiddenException(string message) : base(message)
        {
        }
    }
}
