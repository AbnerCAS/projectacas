﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewACAS.API.Common.Exception
{
    public class NotFoundException : System.Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}
