﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace NewACAS.Data.Access.Maps.Interface
{
    public interface IMap
    {
        void Visit(ModelBuilder builder);
    }
}
