﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using NewACAS.API.Models;
using NewACAS.Data.Access.Maps.Interface;

namespace NewACAS.Data.Access.Maps.Maps
{
    public class CustomerMap : IMap
    {
        public void Visit(ModelBuilder builder)
        {
            builder.Entity<Customer>()
                .ToTable("Customer")
                .HasKey(x => x.Id);
        }
    }
}
