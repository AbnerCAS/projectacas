﻿using System;

namespace NewACAS.API.Models
{
    public class Order
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public bool IsDeleted { get; set; }
    }
}
