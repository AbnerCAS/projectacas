﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewACAS.API.Models.Common
{
    public class DataResult<T>
    {
        public T[] Data { get; set; }
        public int Total { get; set; }
    }
}
