﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewACAS.Data.Model.Order
{
    public class OrderModel
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedAt { get; set; }
        public int CustomerId { get; set; }
    }
}
