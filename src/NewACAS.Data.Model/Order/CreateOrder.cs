﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NewACAS.Data.Model.Order
{
    public class CreateOrder
    {
        public int Id { get; set; }
        public decimal Price { get; set; }
        public string ProductType { get; set; }
        public string ProductName { get; set; }
        public DateTime CreatedAt { get; set; }
        [Required]
        public int CustomerId { get; set; }
    }
}
