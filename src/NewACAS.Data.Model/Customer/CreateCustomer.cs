﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NewACAS.Data.Model.Customer
{
    public class CreateCustomer
    {
        [Required]
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        [Required]
        public string Email { get; set; }
        public string Document { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime BirthDate { get; set; }
        public bool Active { get; set; }
    }
}
