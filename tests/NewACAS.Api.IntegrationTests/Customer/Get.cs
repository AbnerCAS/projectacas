﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NewACAS.Api.IntegrationTests.Common;
using NewACAS.Data.Model.Customer;
using Newtonsoft.Json;
using Xunit;

namespace NewACAS.Api.IntegrationTests.Customer
{
    [Collection("ApiCollection")]
    public class Get
    {
        private readonly ApiServer _server;
        private readonly HttpClient _client;
        private Random _random;

        public Get(ApiServer server)
        {
            _server = server;
            _client = _server.Client;
            _random = new Random();
        }

        [Fact]
        public async Task ReturnCustomerById()
        {
            var item = await new PostShould(_server).CreateNew();

            var result = await GetById(_client, item.Id);

            result.Should().NotBeNull();
        }

        public static async Task<CustomerModel> GetById(HttpClient client, int id)
        {
            var response = await client.GetAsync(new Uri($"api/Customer/{id}", UriKind.Relative));
            response.EnsureSuccessStatusCode();

            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<CustomerModel>(result);
        }

        [Fact]
        public async Task ShouldReturn404StatusIfNotFound()
        {
            var response = await _client.GetAsync(new Uri($"api/Customer/-1", UriKind.Relative));

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

    }
}
