﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NewACAS.Api.IntegrationTests.Common;
using NewACAS.Api.IntegrationTests.Helper;
using NewACAS.Data.Model.Customer;
using Xunit;

namespace NewACAS.Api.IntegrationTests.Customer
{
    [Collection("ApiCollection")]
    public class Post
    {
        private readonly ApiServer _server;
        private readonly HttpClientWrapper _client;
        private Random _random;

        public Post(ApiServer server)
        {
            _server = server;
            _client = new HttpClientWrapper(_server.Client);
            _random = new Random();
        }

        [Fact]
        public async Task<CustomerModel> CreateNew()
        {
            var requestItem = new CreateCustomer()
            {
                FirstName = _random.Next().ToString(),
                CreatedAt = DateTime.Now,
                Email = _random.Next().ToString()
            };

            var createdItem = await _client.PostAsync<CustomerModel>("api/Expenses", requestItem);

            createdItem.Id.Should().BeGreaterThan(0);
            createdItem.FirstName.Should().Be(requestItem.FirstName);
            createdItem.CreatedAt.Should().Be(requestItem.CreatedAt);
            createdItem.Email.Should().Be(requestItem.Email);

            return createdItem;
        }

    }
}
