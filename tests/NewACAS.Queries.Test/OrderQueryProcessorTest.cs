﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NewACAS.API.Common.Exception;
using NewACAS.API.Models;
using NewACAS.Data.Access.DAL;
using NewACAS.Data.Model.Order;
using NewACAS.Queries.Queries;
using Xunit;

namespace NewACAS.Queries.Test
{
    public class OrderQueryProcessorTest
    {
        private Mock<IUnitOfWork> _uow;
        private List<Order> _orderList;
        private IOrderQueryProcessor _query;
        private Random _random;
        private Customer _currentUser;

        public OrderQueryProcessorTest()
        {
            _random = new Random();
            _uow = new Mock<IUnitOfWork>();

            _orderList = new List<Order>();
            _uow.Setup(x => x.Query<Order>()).Returns(() => _orderList.AsQueryable());

            _currentUser = new Customer() { Id = _random.Next() };

            _query = new OrderQueryProcessor(_uow.Object);
        }

        [Fact]
        public void GetShouldReturnOnlyUserExpenses()
        {
            _orderList.Add(new Order { CustomerId = _random.Next() });
            _orderList.Add(new Order { CustomerId = _currentUser.Id });

            var result = _query.Get().ToList();
            result.Count().Should().Be(1);
            result[0].CustomerId.Should().Be(_currentUser.Id);
        }


        [Fact]
        public void GetShouldReturnAllExceptDeleted()
        {
            _orderList.Add(new Order { CustomerId = _currentUser.Id });
            _orderList.Add(new Order { CustomerId = _currentUser.Id, IsDeleted = true });

            var result = _query.Get();
            result.Count().Should().Be(1);
        }

        [Fact]
        public void GetShouldReturnById()
        {
            var expense = new Order { Id = _random.Next(), CustomerId = _currentUser.Id };
            _orderList.Add(expense);

            var result = _query.Get(expense.Id);
            result.Should().Be(expense);
        }

        [Fact]
        public async Task CreateShouldSaveNew()
        {
            var model = new CreateOrder
            {
                ProductName = _random.Next().ToString(),
                Price = _random.Next(),
                ProductType = _random.Next().ToString(),
                CreatedAt = DateTime.Now,
                CustomerId = _currentUser.Id
            };

            var result = await _query.Create(model);

            result.ProductName.Should().Be(model.ProductName);
            result.Price.Should().Be(model.Price);
            result.ProductType.Should().Be(model.ProductType);
            result.CreatedAt.Should().BeCloseTo(model.CreatedAt);
            result.CustomerId.Should().Be(_currentUser.Id);

            _uow.Verify(x => x.Add(result));
            _uow.Verify(x => x.CommitAsync());
        }


        [Fact]
        public async Task DeleteShouldMarkAsDeleted()
        {
            var user = new Order() { Id = _random.Next(), CustomerId = _currentUser.Id };
            _orderList.Add(user);

            await _query.Delete(user.Id);

            user.IsDeleted.Should().BeTrue();

            _uow.Verify(x => x.CommitAsync());
        }

        [Fact]
        public async Task DeleteShouldThrowExceptionIfItemIsNotBelongTheUser()
        {
            var expense = new Order() { Id = _random.Next(), CustomerId = _random.Next() };
            _orderList.Add(expense);

            Action execute = () =>
            {
                _query.Delete(expense.Id).Wait();
            };

            execute.Should().Throw<NotFoundException>();
        }

        [Fact]
        public void DeleteShouldThrowExceptionIfItemIsNotFound()
        {
            Action execute = () =>
            {
                _query.Delete(_random.Next()).Wait();
            };

            execute.Should().Throw<NotFoundException>();
        }
    }
}
